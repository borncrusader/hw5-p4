TEAM INFORMATION
1. Harish Udaiya Kumar (hudaiya)
2. Srinath Krishna Ananthkrishnan (sananth5)
3. Sudarshan Nallan Chakravarthy (snallan)

This is a readme file to explain the organization of the code. The other files included for this assignment are server.c, client.c, common.c, list.c, list.h, myatomic.h, and a Makefile. Initially server instances are run using the executable server. Later client is started using the executable client. The operations involved are GET and PUT. The procedure involved for the various parts of the assignment is briefly explained below.

1. LIAR HANDLING
1.1Assumptions
1. Liar(s) can only be found if majority of the servers are non-liars bearing identical key value and versions. 
2. Liar(s) can lie only about value(s) in GET.
3. Liar(s) may lie about version and values in PUT.
4. If major number of servers (>=N/2) are liars, then GET/PUT may work but would not produce appropriate results. This is because in this case, there is no way to differentiate between liars and non-liars.
5. If N is the number of valid votes, majority is treated as (N/2+1) servers bearing identical version numbers, key and value pairs.
6. The client and server have a timeout (owing to network delay). If the reply does not arrive within the timeout, the operations might not work correctly.
7. Initially all requests are PUT. If client issues a GET request for a non-existent key, server responds with a TYPE_ERROR message. 

1.2 Process
1. Client access servers randomly and initiate GET/PUT operations.
2. Client polls each server which has replied with vote, asking for version number.
3. Server replies with version number, key and value pairs.
4. Client determines the highest version number from these and checks whether this version number is common to majority of servers.  While doing so, it also checks the key and value pair to ascertain whether the current server is the liar or not. If this version number is not available across majority of servers, then client reports the liar and its information. The liar server�s information would not be considered for future comparisons. However it would be updated if PUT operation is successful. The next highest version number is chosen and tested for the same condition. This works exactly according to the assumption 5.
5. If the majority of version numbers, key and value pairs do not match, the code would explicitly prompt possible liar has been reported and GET/PUT might not work appropriately due to the ambiguity in finding liars.

2. LOCKING 
1. For both GET and PUT operation, server checks whether the requested key is already locked and replies accordingly to the client. If the key is locked, it would report in the console and closes the socket.
2. For PUT, server first acquires the lock for the key and then sends the vote to the client. The lock is released either after a predefined timeout (15 seconds) value or after client appropriately sends message to release the lock.
3. Every k will have a separate lock and the key-value data structure has a global lock. The locks are set using compare and swap instruction. 

3. INPUT FILE 

3.1 Server_loc.txt
Every server instance appends an entry containing its hostname and port number to the server_loc.txt file. It is assumed that the client and server are on the same shared directory. Both client and the server reads/updates the file by loading it from the present working directory. 
Client�s request
Client sends requests to servers by randomly reading from the server_loc.txt file to simulate distributed requests. 

4. MESSAGE FORMATS
The following message formats are used to validate valid votes, non-votes, errors, locks etc.
  Client to server 
  TYPE_GET                   - Initial GET request message     
  TYPE_PUT                   - Initial PUT request message
  TYPE_GET_VERSION           - To request version number   
  TYPE_GET_VALUE             - To request values for PUT   
  TYPE_UPDATE                - To update values for PUT
  TYPE_RLS_LOCK              - To release lock after PUT  
 
  Server to client
  TYPE_VOTE                  - To specify a valid vote  
  TYPE_NOT_VOTE              - To specify not to vote 
  TYPE_PUT_VERSION           - Response for TYPE_GET_VERSION
  TYPE_PUT_VALUE             -  TYPE_GET_VALUE
  TYPE_SUCCESS               - Successful update
  TYPE_LOCKED                - If the key is already locked 
  TYPE_ERROR                 - Response for a GET request for a non-existent key 
