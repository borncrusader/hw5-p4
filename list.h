/* Single Author Info:
 *
 * sananth5   Srinath Krishna Ananthakrishnan
 *
 * Group Info:
 *
 * hudaiya    Harish Udaiya Kumar
 * sananth5   Srinath Krishna Ananthakrishnan
 * snallan    Sudarshan Nallan Chakravarthy
 */

#ifndef __LIST_H__
#define __LIST_H__

/* functions
 * LIST_INIT         - call when first creating a list
 *                     will malloc the list_head for use by the API
 * LIST_ELEMENT_INIT - initialises the element to NULL
 * LIST_ADD          - adds the element to the last of the list with key as 0
 * LIST_ADD_KEY      - adds the element to the last of the list with specified key
 * LIST_DELETE_KEY   - removes the element with the specified key
 * LIST_GET_KEY      - gets the element with the specified key
 * LIST_GET_FIRST    - gets the first list_element of the list
 * LIST_GET_NEXT     - gets the list_element next to the specified element
 * LIST_GET_PREV     - gets the list_element previous to the specified element
 * LIST_LENGTH       - returns the length of the list
 * DEQUEUE           - removes the first element and returns it
 * ENQUEUE           - adds the element to the back of the list
 * LIST_DEINIT       - call when the list is done with
 *                     will free everything
 *                     not responsible for freeing the individual elements
 *
 * working
 * - most functions will return 0 on TRUE and non-zero on ERROR
 * - no synchronisation/atomic activity ensurance is provided
 */

typedef struct list_element
{
  int                  key;
  long                 value;
  int                  version;
  int 		       lock;	
  struct list_element *next;
  struct list_element *prev;
}list_element_t;

typedef struct list_head
{
  int length;

  struct list_element *first;
  struct list_element *last;
}list_head_t;

int list_init(list_head_t **list_head);
int list_element_init(list_element_t *list_elem);
int list_add_key(list_head_t *list_head, int key, long value, int version);
int list_get_key(list_head_t *list_head, int key, list_element_t **element);
int list_set_key(list_head_t *list_head, int key, long value, int version);
int list_delete_key(list_head_t *list_head, int key);
list_element_t* list_get_first(list_head_t *list_head);
list_element_t* list_get_next(list_element_t *list_elem);
list_element_t* list_get_prev(list_element_t *list_elem);
int list_deinit(list_head_t **list_head);

//list_head_t *head = NULL;
//int list_lock = 0;
#define LIST_LENGTH(_x) (_x)->length

#endif
