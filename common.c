//TEAM INFORMATION
////1. Harish Udaiya Kumar (hudaiya)
////2. Srinath Krishna Ananthkrishnan (sananth5)
////3. Sudarshan Nallan Chakravarthy (snallan)
#include "p4.h"

unsigned int drawrandom()
{
  static int fd = -1;
  unsigned int rand;

  if (fd == -1) {
    fd = open("/dev/urandom", 0);
  }

  read(fd, &rand, 4);

  return rand;
}

int shuffle(void *a, int size, int n)
{
  void *temp;
  int rand;

  temp = malloc(size);

  if (!a) {
    return 1;
  }

  while (n > 0) {
    rand = drawrandom()%n;
    memcpy(temp, a+rand*size, size);
    memcpy(a+rand*size, a+(n-1)*size, size);
    memcpy(a+(n-1)*size, temp, size);

    n--;
  }

  free(temp);

  return 0;
}

int vote_or_not(float prob)
{
  int rand = drawrandom()%100;

  return rand<((int)(prob*100));
}
