a4 p4: server client

server: server.c list.o common.o
	gcc -g server.c list.o common.o -lpthread -o server

client: client.c list.o common.o
	gcc -g client.c list.o common.o -o client

common.o: common.c
	gcc -c -g common.c -o common.o

list.o: list.c
	gcc -c -g list.c -o list.o

list: list.o
	gcc -g list_test_1.c list.o -o list_test_1

clean:
	rm -rf server client list.o common.o list_test_1
