/* Single Author Info:
 *
 * sananth5   Srinath Krishna Ananthakrishnan
 *
 * Group Info:
 *
 * hudaiya    Harish Udaiya Kumar
 * sananth5   Srinath Krishna Ananthakrishnan
 * snallan    Sudarshan Nallan Chakravarthy
 */

#include <stdlib.h>
#include "list.h"

int list_init(list_head_t **list_head)
{
  if(list_head == NULL) {
    return 1;
  }

  *list_head = (list_head_t*)malloc(sizeof(list_head_t));

  if(!(*list_head)) {
    return 2;
  }

  (*list_head)->length = 0;

  (*list_head)->first = NULL;
  (*list_head)->last = NULL;

  return 0;
}

int list_element_init(list_element_t *list_elem)
{
  if(list_elem == NULL) {
    return 1;
  }

  list_elem->key = 0;
  list_elem->value = 0;
  list_elem->version = 0;

  list_elem->next = NULL;
  list_elem->prev = NULL;

  return 0;
}

int list_add_key(list_head_t *list_head, int key, long value, int version)
{
  list_element_t *c_list_elem = NULL;

  if(!list_head) {
    return 1;
  }

  c_list_elem = (list_element_t*) malloc(sizeof(list_element_t));
  if(!c_list_elem) {
    return 2;
  }

  list_element_init(c_list_elem);
  
  c_list_elem->key = key;
  c_list_elem->value = value;
  c_list_elem->version = version;
  c_list_elem->lock = 1;

  if(!list_head->first) {
    list_head->first = c_list_elem;
  } else {
    list_head->last->next = c_list_elem;
    c_list_elem->prev = list_head->last;
  }

  list_head->length++;
  list_head->last = c_list_elem;

  return 0;
}

int list_set_key(list_head_t *list_head, int key, long value, int version)
{
  list_element_t *t_list_elem = NULL;
  int ret = 1;

  if(!list_head)
    return ret;

  if(list_get_key(list_head, key, &t_list_elem) == 0)
  {
    (t_list_elem)->value = value;
    (t_list_elem)->version = version;
    ret = 0;
  }

  return ret;
}

int list_get_key(list_head_t *list_head, int key, list_element_t **element)
{
  list_element_t *t_list_elem = NULL;

  if(!list_head || !element) {
    return 1;
  }
  
  t_list_elem = list_head->first;

  while(t_list_elem) {
    if(t_list_elem->key == key) {
      *element = t_list_elem;
      return 0;
    }
    t_list_elem = t_list_elem->next;
  }

  return 1;
}

int list_delete_key(list_head_t *list_head, int key)
{
  list_element_t *t_list_elem = NULL;
  list_element_t *prev_elem, *next_elem;

  if(!list_head) {
    return 1;
  }

  t_list_elem = list_head->first;

  while(t_list_elem) {
    if(t_list_elem->key == key) {
      prev_elem = t_list_elem->prev;
      next_elem = t_list_elem->next;

      if(prev_elem) {
        prev_elem->next = next_elem;
      } else {
        // first element is removed
        list_head->first = next_elem;
      }
      if(next_elem) {
        next_elem->prev = prev_elem;
      } else {
        // last element is removed
        list_head->last = prev_elem;
      }

      free(t_list_elem);

      list_head->length--;
      return 0;
    }

    t_list_elem = t_list_elem->next;
  }

  return 1;
}

list_element_t* list_get_first(list_head_t *list_head)
{
  if(!list_head) {
    return NULL;
  }

  return list_head->first;
}

list_element_t* list_get_next(list_element_t *list_elem)
{
  if(!list_elem) {
    return NULL;
  }

  return list_elem->next;
}

list_element_t* list_get_prev(list_element_t *list_elem)
{
  if(!list_elem) {
    return NULL;
  }

  return list_elem->prev;
}

int list_deinit(list_head_t **list_head)
{
  list_element_t *t_list_elem = NULL;
  list_element_t *c_list_elem = NULL;

  if(!list_head || !*list_head) {
    return 1;
  }

  t_list_elem = (*list_head)->first;

  while (t_list_elem) {
    c_list_elem = t_list_elem->next;
    free(t_list_elem);
  }

  free(*list_head);
  *list_head = NULL;

  return 0;
}
