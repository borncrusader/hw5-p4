//TEAM INFORMATION
////1. Harish Udaiya Kumar (hudaiya)
////2. Srinath Krishna Ananthkrishnan (sananth5)
////3. Sudarshan Nallan Chakravarthy (snallan)
#include "p4.h"

struct server_info
{
  char *hostname;
  int port;
  int socket;
  int contact; 
  int key;
  long value;
  unsigned int version;
  int liar;
};

typedef struct
{
  int N;
  int NR;
  int NW;

  char opr; // 0 - GET, 1 - PUT, 2- FORCE PUT (for final update)

  int key;
  long value;

  struct server_info* sinfo;
}client_t;

void usage(char *file)
{
  fprintf(stderr, "usage: %s [N] [NR] [NW] [PUT/GET] [key] [value]\n", file);
  exit(1);
}

int is_liar(client_t *client, int ind)
{
  int i, ver, key, val, ver_cnt, key_val_cnt, liar = -1;

  ver_cnt = 1;
  key_val_cnt = 1;

  ver = (client)->sinfo[ind].version;
  key = (client)->sinfo[ind].key;
  val = (client)->sinfo[ind].value;

  for(i=0;i<(client)->N && (client)->sinfo[i].contact; i++)
  {
    if(i == ind)
      continue;
      
    //printf("\ninside for loop i:%d ind:%d",i,ind);     
    if ((client)->sinfo[i].version == ver) {
      //printf("version match\n");
      ver_cnt++;

      if((client)->sinfo[i].key == key && (client)->sinfo[i].value == val) {
        //printf("key value match\n");
        key_val_cnt++;         
      } else {
        liar = i;
      } 
    }    
  }

  // Majority check - if satisfied this is not the liar
  if((key_val_cnt != 1) && (ver_cnt != 1) && (key_val_cnt > (ver_cnt/2)))
  {
    if (liar != -1) {
      printf("Liar! host:%s version:%d key:%d value:%ld\n",(client)->sinfo[liar].hostname,(client)->sinfo[liar].version,
        (client)->sinfo[liar].key,(client)->sinfo[liar].value);
      (client)->sinfo[liar].version = 0;
    }
    return 0;
  }
  //printf("ver_cnt:%d key_val_cnt:%d\n",ver_cnt,key_val_cnt); 
  return 1; 
}

int get_server_ind(client_t *client, int ver)
{
  int i;

  for(i=0;((i<client->N) && (client->sinfo[i].contact !=0) && (client->sinfo[i].version != 0)); i++)
  {
    if(client->sinfo[i].version == ver)
      return i; 
  }
   
  return 0;
}
 
int get_next_version(client_t *client)
{
  // 1. Find the highest version number & key value pair
  // 2. Find the number of servers bearing this triad and identify the liar
  int i, j, max, ind, num_votes, def_ver_cnt, liar_count, ret;
  liar_count = 0;
  for(i=0;i<client->N && client->sinfo[i].contact !=0; i++)
  {
    num_votes = 0;
    def_ver_cnt = 0; 
    ind  = i; 
    max = client->sinfo[i].version;
    //printf("max1:%d\n",max);
    /* Find the highest version number of this iteration */
    for(j=0;j<client->N && client->sinfo[j].contact !=0; j++)
    {     
      if(i == j)
        continue;

      if (client->sinfo[j].version != 0) {
        if (max < client->sinfo[j].version) {
          max = client->sinfo[j].version;
          ind = j;
        }
      }
    }
   
    //printf("max2:%d ind:%d\n",max,ind);
    /* If the server lied, discard this version-key-value triad and proceed with the next highest version*/
    if((ret = is_liar(client, ind)) == 1)
    {
      printf("Possible liar! host:%s version:%d key:%d value:%ld.May result in incorrect update\n",client->sinfo[ind].hostname,
        client->sinfo[ind].version,client->sinfo[ind].key,client->sinfo[ind].value);
      client->sinfo[ind].version = 0; 
      //exit(0);
      //printf("liar version:%d\n",client->sinfo[ind].version);
      liar_count++;       
    }

    //printf("max3:%d liar:%d\n",max,liar_count);
    if (((client->N % 2 == 0) && (liar_count >= client->N/2)) || ((client->N % 2 != 0)&&(liar_count > client->N/2))) {         
        printf("Majority of the servers have key-value-version mismatch. May lead to incorrect GET/PUT. Terminating!\n");  
        exit(1);
    }
    if(ret)
      continue;  
   
    //*serv = ind; 
    return (max+1);
  }
}

void read_server_list(client_t *client)
{
  FILE *fp = NULL;
  char buf[BUF_SIZE];
  char *s;
  int i = 0, len;

  client->sinfo = malloc(client->N * sizeof(struct server_info));

  fp = fopen(SERVER_LOC, "r");

  if (!fp) {
    fprintf(stderr, "file server_loc.txt not found!\n");
    exit(1);
  }

  while (fgets(buf, BUF_SIZE-1, fp)) {
    s = strtok(buf, " ");
    len = strlen(s);

    if (i == client->N) {
      fprintf(stderr, "num servers > %d", client->N);
      exit(1);
    }

    client->sinfo[i].hostname = malloc(len+1);
    strncpy(client->sinfo[i].hostname, s, len);

    s = strtok(NULL, " ");
    client->sinfo[i].port = atoi(s);
    client->sinfo[i].contact = 1;
    client->sinfo[i].version = 0;

    i++;
  }

  /* chances of the supplied N being greater than the number of entries */
  client->N = i;

  fclose(fp);

  shuffle(client->sinfo, sizeof(struct server_info), client->N);

}

int main(int argc, char *argv[])
{
  client_t client;
  char buf[BUF_SIZE], *str, *temp_str;
  struct addrinfo hints, *serv_info, *p;
  int nbytes, ret, i, num_votes;
  int next_version, unlock_flag;
  msg_type msg;
  struct timeval tv;
  
  if (argc < 5) {
    usage(argv[0]);
  }

  memset(&client, 0, sizeof(client_t));

  if (!strncmp(argv[4], "GET", 3)) {
    client.opr = TYPE_GET;
  } else if (!strncmp(argv[4], "PUT", 3)) {
    client.opr = TYPE_PUT;
  } else {
    usage(argv[0]);
  }

  if (((client.opr == TYPE_PUT) && argc < 7) ||
      ((client.opr == TYPE_GET) && argc < 6)) {
    usage(argv[0]);
  }

  client.N = atoi(argv[1]);
  client.NR = atoi(argv[2]);
  client.NW = atoi(argv[3]);
  client.key = atoi(argv[5]);
  if (client.opr == TYPE_PUT) {
    client.value = atol(argv[6]);
  }

  if ((client.NR + client.NW) <= client.N) {
    printf("NR+NW > N check failed!\n");
    exit(0);
  } else if (client.NW <= client.N/2) {
    printf("NW > N/2 check failed!\n");
    exit(0);
  }

  read_server_list(&client);

  tv.tv_sec = RECV_TIMEOUT + 5;
  tv.tv_usec = 0;

  //next_version = 1;
  memset(&hints, 0, sizeof(hints));
  hints.ai_family = AF_UNSPEC;
  hints.ai_socktype = SOCK_STREAM;
  for (i=0; i < client.N; i++) {
    //printf("sending to host : %s on port %d\n", client.sinfo[i].hostname, client.sinfo[i].port); 
    if ((ret = getaddrinfo(client.sinfo[i].hostname, NULL, &hints, &serv_info)) != 0) {
      fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(ret));
      return 1;
    }
    for (p = serv_info; p != NULL; p = p->ai_next) {
      if ((client.sinfo[i].socket = socket(p->ai_family, p->ai_socktype,
              p->ai_protocol)) == -1) {
        perror("client: socket");
        continue;
      }

       if (setsockopt(client.sinfo[i].socket, SOL_SOCKET, SO_RCVTIMEO, (char *)&tv,  sizeof tv)) {
         perror("setsockopt failed: ");
         exit(1);
       }       
   
      ((struct sockaddr_in*)(p->ai_addr))->sin_port = htons(client.sinfo[i].port);

      if (connect(client.sinfo[i].socket, p->ai_addr, p->ai_addrlen) == -1) {
        close(client.sinfo[i].socket);
        perror("client: connect");
        continue;
      }
      break;
    }
    if (p == NULL) {
      fprintf(stderr, "client: failed to connect\n");
      return 1;
    }
    freeaddrinfo(serv_info);
  }

  /* sending operation to the guys */
  for (i=0; i < client.N; i++) {
    //printf("key:%d value:%ld opr:%d",client.key,client.value,client.opr);   
    sprintf(buf, "%d:%d:%ld", client.opr, client.key, client.value); 
    //printf("sending : %s\n", buf); 
    if ((nbytes = send(client.sinfo[i].socket, buf, strlen(buf)+1, 0)) == -1) {
      perror("client : send");
      return 1;
    }
  }

  /* receiving votes/non-votes */
  num_votes = 0;
  for (i=0; i < client.N; i++) {
    ret = recv(client.sinfo[i].socket, buf, BUF_SIZE-1, 0);
    buf[ret] = '\0';
    
    if (buf/*ret != 0 && ret != -1*/) {
      if ((atoi(buf) == TYPE_NOT_VOTE) || (atoi(buf) == TYPE_ERROR) || (atoi(buf) == TYPE_LOCKED)) {
        client.sinfo[i].contact = 0;     
        printf("not vote received from <%s> <%d>\n",client.sinfo[i].hostname, client.sinfo[i].port);
      }
      else {
        printf("vote received from <%s> <%d>\n", client.sinfo[i].hostname,client.sinfo[i].port);
        num_votes++;
      }
    }
  }
  
  /* check condition again */
  if (client.opr == TYPE_PUT && num_votes < client.NW) {
    printf("Number of votes:%d is lesser than NW:%d\nExiting!",num_votes,client.NW);
    exit(1);
  }
  if (client.opr == TYPE_GET && num_votes < client.NR) {
    printf("Number of votes:%d is lesser than NR:%d\n Exiting!",num_votes,client.NR);
    exit(1);
  }
  
  /* ask version number from the guys */
  for (i=0; i < client.N && client.sinfo[i].contact; i++) {
    /* If the server has not voted, skip from contacting it */
    sprintf(buf, "%d", TYPE_GET_VERSION);
    //printf("sending : %s\n", buf); 
    if ((nbytes = send(client.sinfo[i].socket, buf, strlen(buf)+1, 0)) == -1) {
      perror("client : send");
      return 1;
    }
  }

  /* receiving version numbers and values */
  for (i=0; i < client.N && client.sinfo[i].contact; i++) {
    //sleep(2);
    ret = recv(client.sinfo[i].socket, buf, BUF_SIZE-1, 0);
    buf[ret] = '\0';

    if (ret != -1 || ret != 0) {      
      str = (char *) malloc(strlen(buf));
      temp_str = str;
      str = strtok(buf,":");
      if(str) 
        msg = atoi(str);   
      if(msg == TYPE_PUT_VERSION) {
        str = strtok(NULL,":");
        if(str) 
          client.sinfo[i].version = atoi(str);

        str = strtok(NULL,":");
        if(str)         
          client.sinfo[i].key = atoi(str);
        
        str = strtok(NULL,":");
        if(str)
          client.sinfo[i].value = atol(str); 
        //printf("version no:<%d> at <%s> <%d> key:%d value:%ld\n",client.sinfo[i].version, 
        //  client.sinfo[i].hostname,client.sinfo[i].port, client.sinfo[i].key, client.sinfo[i].value);
        printf("version no:<%d> at <%s> <%d>\n",client.sinfo[i].version, client.sinfo[i].hostname,client.sinfo[i].port);
        free(temp_str);
      } 
    }
  }

  /* write value to guys with highest version number */
  unlock_flag = 1;
  int serv = 0;
  next_version = get_next_version(&client);
  if (client.opr == TYPE_PUT) {
    for (i=0; i < client.N && client.sinfo[i].contact; i++) {
      sprintf(buf, "%d:%d", TYPE_UPDATE, next_version);
      printf("updating Key:<%d> with Version <%d> Value <%d> at <%s> <%d>\n",client.key, next_version, client.value, 
        client.sinfo[i].hostname, client.sinfo[i].port); 
     
      // --- TESTING CONCURRENT LOCKING ---     
      if(argv[7])
        sleep(atoi(argv[7]));  
      // ---- END ----
      if ((nbytes = send(client.sinfo[i].socket, buf, strlen(buf)+1, 0)) == -1) {
        perror("client : send");
        return 1;
      }
    }
    
    /* success from servers */
    for (i=0; i < client.N && client.sinfo[i].contact; i++) {
      ret = recv(client.sinfo[i].socket, buf, BUF_SIZE-1, 0);
      buf[ret] = '\0';
      //printf("received : %s\n", buf);     
      if (atoi(buf) != TYPE_SUCCESS) {
        unlock_flag = 0; 
      }
    }
    
    /* Send message to all to release the lock */
    if (unlock_flag) {
      for (i=0; i < client.N && client.sinfo[i].contact; i++) {
        sprintf(buf, "%d:", TYPE_RLS_LOCK);
        //printf("sending : %s\n", buf);
        if ((nbytes = send(client.sinfo[i].socket, buf, strlen(buf)+1, 0)) == -1) {
          perror("client : send");
          return 1;
        }
      }
    }

  } else { // client.opr == TYPE_GET
    /* Request value from the server with the highest version number */

    /*for(i=0; (i < client.N) && client.sinfo[i].contact != 0; i++) {
      if (client.sinfo[i].version == next_version - 1) {
        break; 
      }         
    }*/
    //printf("i:%d next_version:%d",i,next_version);
    serv = get_server_ind(&client, next_version - 1);
    //if(serv == 0)
    //  printf("correct server not traceable\n");

    sprintf(buf,"%d", TYPE_GET_VALUE);
    if ((nbytes = send(client.sinfo[serv].socket, buf, strlen(buf)+1, 0)) == -1) {
      perror("client : send");
      return 1;
    }
    printf("highest version no at <%s><%d>\n",client.sinfo[serv].hostname, client.sinfo[serv].port);

    /* Read the value from the server */
    ret = recv(client.sinfo[serv].socket, buf, BUF_SIZE-1, 0);
    buf[ret] = '\0';    
    //printf("value from the server:%s\n",buf);  
    if(ret != -1 || ret != 0) {
      str = malloc(strlen(buf));
      temp_str = str;  
      //strcpy(temp, strtok(buf,":"));
      str = strtok(buf,":");
      if(str)
        client.opr = atoi(str);
      //strcpy(temp, strtok(NULL,":"));      
      
      str = strtok(NULL,":");
      if(str) 
        client.value = atol(str);       

      if(client.opr == TYPE_PUT_VALUE) {
        printf("Reading key/value: <%d>/<%ld>\n",client.key,client.value);
      }
      free(temp_str);
    }
  }
  //printf("getchar\n");getchar();
  /* done! */
  for (i=0; i < client.N; i++) {
    close(client.sinfo[i].socket);
  }

  return 0;
}
