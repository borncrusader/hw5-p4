//TEAM INFORMATION
////1. Harish Udaiya Kumar (hudaiya)
////2. Srinath Krishna Ananthkrishnan (sananth5)
////3. Sudarshan Nallan Chakravarthy (snallan)

#include "p4.h"
#include "list.h"
#include "myatomic.h"

typedef struct
{
  int port;
  float prob;
  int lier;
  int socket;
}server_t;

typedef struct thread
{
  int sock;
  server_t server; 
  struct sockaddr_in sender;
 
}thread_data;

server_t server;
list_head_t *head = NULL;
int list_lock = 0;

int lock(int *var)
{
  if (!var) {
    return 1;
  }
  
  while(1) {
    while(*var != 0);  
    if(0 == compare_and_swap(var, 1, 0)) {
      return 0;
    }
  }

  return 1;
}

int unlock(int *var)
{
  if(!var) {
    return 1;
  }
  *var = 0;
  return 0;
}

void *worker_thread(void* args)
{
  char buf[BUF_SIZE], operation[OPR_SIZE], *str, *temp_str;
  int ret, msg, opr, key, version, old_value, vote, new_version, aso; 
  int nbytes, lret, next_version;
  long value;
  struct timeval tv;
  struct hostent *he;
  list_element_t *cur = NULL;
  thread_data *thr = (thread_data *) args;  

  //printf("sock:%d",aso);
  aso = thr->sock; 
  /* Set socket option for timeout */
    tv.tv_sec = RECV_TIMEOUT;
    tv.tv_usec = 0;
    if (setsockopt(aso, SOL_SOCKET, SO_RCVTIMEO, (char *)&tv,  sizeof tv)) {
        perror("setsockopt failed: ");
        exit(1);
    } 

    /* initial operation retrieve */
    ret = recv(aso, buf, BUF_SIZE-1,  0);
    buf[ret] = '\0';
    //printf("received %d bytes of message %s\n",ret, buf); 
    if (ret != -1 || ret != 0) {
      //printf("buf:%s",buf);  
      str = malloc(strlen(buf));
      temp_str = str;
      str = strtok(buf, ":"); 
      if(str) {
        opr = atoi(str); //printf("opr:%d",opr);
      }
 
      str = strtok(NULL, ":");
      if(str) {
        key = atoi(str);//printf("key:%d",key);
      }

      str = strtok(NULL, ":");
      if(str) {
        value = atol(str); //printf("value:%ld",value);
      }  
    }
 
    if(opr == TYPE_GET)
      strcpy(operation, "GET"); 
    else
      strcpy(operation, "PUT"); 

    operation[OPR_SIZE - 1] = '\0'; 
    he = gethostbyaddr(&(thr->sender.sin_addr), sizeof(thr->sender.sin_addr), AF_INET);
    printf("contacted by <%s><%d> for %s <%d>\n",he->h_name, thr->sender.sin_port, operation, key);
    if ((lret = list_get_key(head, key, &cur)) == 0) {
      /* If the entry is already locked, send a message to the client */
      if (cur->lock) {
        sprintf(buf, "%d", TYPE_LOCKED);
	if ((nbytes = send(aso, buf, strlen(buf)+1, 0)) == -1) {
          perror("server : send");
          return;
        }
        printf("Key entry already locked, can't give access, closing sock\n");  
        close(aso);
        return;
      }   

      /* key is in the list indeed! */
      version = cur->version;
      old_value = cur->value;
      vote = vote_or_not(server.prob);
    } else {
      /* if not present, reply with TYPE_ERROR */
      if (opr == TYPE_GET) {
         sprintf(buf, "%d", TYPE_ERROR);
         if ((nbytes = send(aso, buf, strlen(buf)+1, 0)) == -1) {
           perror("server : send");
           return;
         } 
         close(aso);
         return; 

      } else if (opr == TYPE_PUT) {
        version = 0;
        vote = 1;
      }
    }

    /* send vote or not */
    if (vote) {
      /* take lock for TYPE_PUT */
      if (opr == TYPE_PUT && lret == 0) {
        lock(&cur->lock);
        printf("lock on <%d>\n", key);
      }
      sprintf(buf, "%d", TYPE_VOTE);
    } else {
        sprintf(buf, "%d", TYPE_NOT_VOTE);
    }

    if ((nbytes = send(aso, buf, strlen(buf)+1, 0)) == -1) {
      perror("server : send");
      return;
    }

    /* client asks for version number */
    ret = recv(aso, buf, BUF_SIZE-1, 0);
    buf[ret] = '\0';
    //printf("received %d bytes of message %s\n",ret, buf); 
    msg = atoi(buf);

    /* send version number */
    if (msg == TYPE_GET_VERSION) {
      
      if(opr == TYPE_PUT && server.lier) {
        version = version + 10;        
      } else if (opr == TYPE_GET && server.lier){       
        old_value = old_value + 100;  
      }

      sprintf(buf, "%d:%d:%d:%ld", TYPE_PUT_VERSION, version, key, old_value);
      if ((nbytes = send(aso, buf, strlen(buf)+1, 0)) == -1) {
        perror("server : send");
        return;
      }
      printf("sending version No <%d> to <%s> <%d>\n", version, he->h_name, thr->sender.sin_port, key, value);
      //printf("sending version No <%d> to <%s> <%d> with key:%d value:%ld\n", version, he->h_name, thr->sender.sin_port, key, value);
      if(server.lier)
        printf("lied to <%s> <%d> about <%d>\n", he->h_name, thr->sender.sin_port, key); 
    }

    if (opr == TYPE_PUT) {
      /* client may send an update */
      ret = recv(aso, buf, BUF_SIZE-1, 0);
      buf[ret] = '\0';
      //printf("received %d bytes of message %s\n",ret, buf); 
      if (ret != -1 && ret != 0) {
        msg = atoi(strtok(buf, ":"));        
      } 

      if (msg == TYPE_UPDATE) {
        //printf("Update msg received from client\n");
        /* update the list */
        next_version = atoi(strtok(NULL, ":"));   
        printf("writing key <%d> value <%ld> with updated version no <%d>\n", key, value, next_version);
        if(lret == 0) {
          //printf("lock call before set key\n");
          lock(&list_lock);
          list_set_key(head, key, value, next_version);
          unlock(&list_lock);  
          //printf("set_key called\n");
        } 
        else {
          //printf("lock call before add key\n");
          lock(&list_lock);
          list_add_key(head, key, value, next_version);
          unlock(&list_lock);  
        }
           
        sprintf(buf, "%d", TYPE_SUCCESS);
        if ((nbytes = send(aso, buf, strlen(buf)+1, 0)) == -1) {
          perror("server : send");
          return;
        }
      }

      //printf("\nbefore blocking on recv\n");
      /* Client may or may not ask to release the lock, handle both cases */ 
      ret = recv(aso, buf, BUF_SIZE-1, 0);
      if(ret != -1 && ret != 0){
        buf[ret] = '\0';
        //printf("received %d bytes of message %s\n",ret, buf);
        msg = atoi(strtok(buf, ":"));  
       // printf("Waiting for release\n");
      }

      if ((ret == -1)||(ret == 0)||(msg == TYPE_RLS_LOCK) || (errno == EAGAIN ) || (errno == EWOULDBLOCK )) {
        //printf("\nbefore list_get_key\n");
        //if(ret == -1)
        //  printf("force release of lock\n");
        if ((lret = list_get_key(head, key, &cur)) == 0) {
          //printf("Inside type_rls_lock \nbefore unlock var:%d\n",cur->lock);
          unlock(&(cur->lock));
          //printf("after unlock var:%d\n",cur->lock);
        }
      }
         
    } else if  (opr == TYPE_GET) {
        /* client requests for the value */       
        ret = recv(aso, buf, BUF_SIZE-1, 0);
        if(ret != -1 && ret != 0){
          buf[ret] = '\0';
          //printf("received %d bytes of message %s\n",ret, buf);
          msg = atoi(buf);//atoi(strtok(buf, ":")); 
          //printf("Before sending value\n"); 
          if (msg == TYPE_GET_VALUE) {
             list_get_key(head, key, &cur);// Condition not needed as the client would have contacted only the appropriate server
             value = cur->value;             
             /* send the value to the client */ 
             sprintf(buf, "%d:%ld", TYPE_PUT_VALUE, value);
             printf("sending key <%d> ,value <%ld> to <%s> <%d> \n", key, old_value, he->h_name, thr->sender.sin_port); 
             if ((nbytes = send(aso, buf, strlen(buf)+1, 0)) == -1) {
               perror("server : send");
               return;
             }
          }
        }
      }
      else {printf("Error!\n");}
    //printf("success!\n");

    close(aso);
}

int main(int argc, char *argv[])
{
  struct sockaddr_in saddr, sender;
  int len, aso;
  FILE* locfile_fp = NULL;
  int tcount = 0;
  long sock_fd;
  char buf[BUF_SIZE];
  pthread_t tid;
  thread_data *thr_data;

  if (argc < 4) {
    fprintf(stderr, "usage: %s [port] [prob-of-voting] [lier]\n", argv[0]);
    exit(1);
  }

  server.port = atoi(argv[1]);
  server.prob = atof(argv[2]);
  server.lier = argv[3][0] - '0';

  list_init(&head);

  if ((server.socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) == -1) {
    perror("socket creation failed: ");
    exit(1);
  }

  saddr.sin_family = AF_INET;
  saddr.sin_addr.s_addr = INADDR_ANY;
  saddr.sin_port = htons(server.port);

  if (bind(server.socket, (struct sockaddr*)&saddr, sizeof(saddr)) == -1) {
    close(server.socket);
    perror("bind failed: ");
    exit(1);
  }

  if (listen(server.socket, LISTEN_BACKLOG) == -1) {
    close(server.socket);
    perror("listen failed: ");
    exit(1);
  }

  /* get the hostname and write it to server_loc.txt */
  gethostname(buf, BUF_SIZE-1);
  locfile_fp = fopen(SERVER_LOC, "a");
  if (locfile_fp == NULL) {
	  fprintf(stderr, "error opening the file : %s\n", SERVER_LOC);
	  exit(1);
  }
  fprintf(locfile_fp, "%s %d\n", buf, server.port);
  fclose(locfile_fp);

  while (1) {
    len = sizeof(sender);
    if ((aso = accept(server.socket, (struct sockaddr*)&sender, &len)) == -1) {
      perror("accept failed: ");
      continue;
    }
    
    thr_data = (thread_data *) malloc(sizeof(thread_data));
    thr_data->sender = sender;
    thr_data->sock = aso;
    thr_data->server = server;
    /* Create a child thread to take care of this client's request */    
    //printf("sock:%d\n",aso);
    pthread_create(&tid, NULL, worker_thread, (void *) thr_data);  
    //pthread_create(&tid, NULL, worker_thread, (void *)&sock_fd );  
    tcount++;
  }
  
  close(server.socket);
  return 0;
}
