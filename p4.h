#ifndef __P4_H__
#define __P4_H__

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <netdb.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <math.h>

#define BUF_SIZE 1400
#define LISTEN_BACKLOG 10
#define SERVER_LOC "server_loc.txt"
#define RECV_TIMEOUT 10
#define OPR_SIZE 4
#define THREAD_COUNT 1024

typedef enum
{
  // client
  TYPE_GET = 0,
  TYPE_PUT,
  TYPE_GET_VERSION,
  TYPE_GET_VALUE,
  TYPE_UPDATE,
  TYPE_RLS_LOCK,

  // server
  TYPE_VOTE = 6,
  TYPE_NOT_VOTE,
  TYPE_PUT_VERSION,  
  TYPE_PUT_VALUE,
  TYPE_SUCCESS,
  TYPE_LOCKED,
  TYPE_ERROR 
}msg_type;

unsigned int drawrandom();
int shuffle(void *a, int size, int n);
int vote_or_not(float prob);

#endif
